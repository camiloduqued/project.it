**Project It.**
---------------

Aplicación web de gestión de proyectos y tareas.

**Requirements**

	-Composer.
	-Vagrant.
	-Laravel.
	-Box: laravel/homestead

**How tu set up laravel project?**

	1. Clone repo.
	2. Navegate into repo folder.
	3. Download dependencies with composer.
		$ composer update
	4.Download vagrant box.
	5.Init vagrant box.
	6.Test in virtual machine.
	
**License**
This project is under a MIT license.
	