<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function(){
    Route::get('/', function (){
        return view('admin.index');
    })->name('admin.index');
    Route::resource('users', 'UsersController');
    Route::resource('projects', 'ProjectsController');
    Route::resource('tasks', 'TasksController');
});

Route::group(['prefix' => 'home', 'middleware' => 'auth'], function(){
    Route::get('/','HomeController@index')->name('home');
    //Route::get('/tasks','HomeController@showTasks')->name('home.tasks');
    //Route::get('/projects','HomeController@showProjects')->name('home.projects');
    Route::resource('/projects', 'MyProjectsController', ['as'=>'main']);
    Route::resource('/tasks', 'MyTasksController', ['as'=>'main']);
    Route::post('/tasks/attachUser', 'MyTasksController@attachUser')->name('main.tasks.attachUser');
    Route::post('/tasks/comment', 'MyTasksController@comment')->name('main.tasks.comment');
    Route::post('/tasks/setProgress', 'MyTasksController@setProgress')->name('main.tasks.setProgress');
    Route::post('/myUser/update', 'MyUserController@update')->name('main.myUser.update');
});

Auth::routes();
