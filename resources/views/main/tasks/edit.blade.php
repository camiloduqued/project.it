@extends('main.home')

@section('title','- '.$task->name)

@section('content')
    <div>
    @if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $e)
            <li>{{$e}}</li>
        @endforeach
    </div>
    @endif
    <div class="forms">
    {!! Form::open(['route' => ['main.tasks.update', $task], 'method' => 'PUT']) !!}
        <div class="form-group">
            {!! Form::label('alias', 'Alias') !!}
            {!! Form::text('alias',$task->alias, ['class' => 'form-control', 'placeholder' => 'Ingrese un alias (único)', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('name',$task->name, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripcion') !!}
            {!! Form::text('description',$task->description, ['class' => 'form-control', 'placeholder' => 'Ingrese la descripción', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Estado') !!}
            {!! Form::text('status',$task->status, ['class' => 'form-control', 'placeholder' => 'Ingrese el estado', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('initial_date', 'Fecha inicial') !!}
            {!! Form::date('initial_date',$task->initial_date, ['class' => 'form-control', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('final_date', 'Fecha final') !!}
            {!! Form::date('final_date',$task->final_date, ['class' => 'form-control', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}
    <div>
        Usuarios asignados: <br>
        @foreach($task->user as $userAttached)
        <h3><span class="badge badge-secondary">
            {{ $userAttached->name }}
        </span></h3>
        @endforeach
        {!! Form::open(['route' => ['main.tasks.attachUser', $task], 'method' => 'POST']) !!}
            {!! Form::hidden('task_id', $task->id) !!}
            <div class="form-group">
                {!! Form::label('user_to_attach', 'Asignar otro usuario a esta tarea') !!}
                {!! Form::select('user_to_attach', $options) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Asignar', ['class'=>'btn btn-primary']) !!}
            </div>
        {!! Form::close() !!}
    </div>
    </div>
    </div>
    <div class="progress_container">
        <h3>Progreso</h3>
        ¿Cuál es tu progreso(horas)?
        <div>
            {!! Form::open(['route' => ['main.tasks.setProgress', $task], 'method' => 'POST']) !!}
            {!! Form::hidden('task_id', $task->id) !!}
            {!! Form::hidden('user_task_id', $user_task->id) !!}
            <div class="form-group">
                {!! Form::number('progress_time', $user_task->progress_time); !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Asignar', ['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
        Progreso general
        <div class="progress_bar_container">
            <div class="percentage">{{$user_task->progress_percentage}}%</div>
            <div class="progress_bar" style="width:{{$user_task->progress_percentage}}">
            </div>
        </div>
    </div>
    <div class="comments">
        @if(count($task->comment) > 0)
            @foreach ($task->comment as $comment)
                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <h5 class="card-title">{{$comment->title}}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{$comment->user->name}}</h6>
                        <p class="card-text">{{$comment->content}}</p>
                        @foreach ($comment->tag as $tag)
                            <span class="badge badge-secondary">
                                {{ $tag->content }}
                            </span>
                        @endforeach
                    </div>
                </div>
                <br>
            @endforeach
        @else
        No hay Comentarios
        @endif
        <div class="create_comment">
            {!! Form::open(['route' => ['main.tasks.comment', $task], 'method' => 'POST']) !!}
            {!! Form::hidden('task_id', $task->id) !!}
            
            <div class="form-group">
                {!! Form::text('title',null,  ['class' => 'form-control', 'placeholder' => 'Ingrese el titulo', 'required'] ) !!}
            </div>
            <div class="form-group">
                {!! Form::textarea('content',null, ['class' => 'form-control', 'placeholder' => 'Comente aquí', 'required', 'rows'=>'4'] ) !!}
            </div>
            <div class="form-group">
                {!! Form::text('tags',null,  ['class' => 'form-control', 'placeholder' => 'Escribe tags separados por comma', 'required'] ) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Comentar', ['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection