@extends('layouts.app')

@section('additional_styles')
<link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('content')
    <div class="main_title title">¿Qué quieres hacer?</div>
@endsection

@section('main_bar')
<div id="main_bar">
    <div class="links">
        <a href="{{ route('main.projects.index') }}">Proyectos</a>
        <a href="{{ route('main.tasks.index') }}">Tareas</a>
    </div>
</div>
@endsection
