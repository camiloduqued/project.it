@extends('main.home')

@section('title', '- Mis proyectos')

@section('content')
<div style="width:100%; padding:10px">
    <div class="main-buttons">
        <form method="get" action="{{route('main.projects.create')}}">
        <button type="submit" class="btn btn-success">
            <img src= {{ @asset('/svg/plus.svg') }} alt="Mis proyectos">
        </button>
        </form>
    </div>
    <hr>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Alias</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripción</th>
                <th scope="col">Estado</th>
                <th scope="col">Fecha inicial</th>
                <th scope="col">Fecha final</th>
                <th scope="col">Editar</th>
                <th scope="col">Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($myProjects as $project)
                <tr>
                    <th scope="row">{{ $project->alias }}</th>
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->description }}</td>
                    <td>{{ $project->status }}</td>
                    <td>{{ $project->initial_date }}</td>
                    <td>{{ $project->final_date }}</td>
                    <td>
                        <a href="{{ route('main.projects.edit', $project->id) }}">
                            <button class="btn btn-info">
                                <img src= {{ @asset('/svg/pencil.svg') }} alt="Editar">
                            </button>
                        </a>
                    </td>
                    <td>
                        <form action="{{ route('main.projects.destroy', $project->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">
                                <img src= {{ @asset('/svg/trash.svg') }} alt="Eliminar">
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection