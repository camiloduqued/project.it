@extends('admin.main')

@section('title', 'Creacion de usuarios')

@section('content')

    @if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $e)
            <li>{{$e}}</li>
        @endforeach
    </div>
    @endif
    
    {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'files' => true]) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('name',null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Correo') !!}
            {!! Form::email('email',null, ['class' => 'form-control', 'placeholder' => 'Ingrese el correo', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('password', 'Contraseña') !!}
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Ingrese su contraseña', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('admin', '¿Es administrador?') !!}
            {!! Form::checkbox('admin'); !!}
        </div>
        <div class="form-group">
            {!! Form::label('avatar', 'Avatar') !!}
            {!! Form::file('avatar'); !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}
@endsection