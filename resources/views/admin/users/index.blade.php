@extends('admin.main')
@section('title', 'Todos los usuarios')

@section('content')
    <div class="main-buttons">
        <form method="get" action="{{route('users.create')}}">
        <button type="submit" class="btn btn-success">
            <img src= {{ @asset('/svg/plus.svg') }} alt="Agregar nuevo usuario">
        </button>
        </form>
    </div>
    <hr>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Avatar</th>
                <th scope="col">Nombre</th>
                <th scope="col">Correo</th>
                <th scope="col">Tareas asignadas</th>
                <th scope="col">Administrador</th>
                <th scope="col">Editar</th>
                <th scope="col">Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td><img class="avatar_img" src="/imgs/user/{{ $user->avatar }}" alt=""></td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @foreach ($user->task as  $asigned_task)
                            {{ $asigned_task->name }},
                        @endforeach
                    </td>
                    <td>
                        @if($user->admin)
                        <img src= {{ @asset('/svg/check.svg') }} alt="Editar">
                        @else
                        <img src= {{ @asset('/svg/x.svg') }} alt="Editar">
                        @endif
                        
                    </td>
                    <td>
                        <a href="{{ route('users.edit', $user->id) }}">
                            <button class="btn btn-info">
                                <img src= {{ @asset('/svg/pencil.svg') }} alt="Editar">
                            </button>
                        </a>
                    </td>
                    <td>
                        <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">
                                <img src= {{ @asset('/svg/trash.svg') }} alt="Eliminar">
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination_links row">
            {{ $users->links() }}
    </div>
@endsection