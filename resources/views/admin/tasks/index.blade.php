@extends('admin.main')
@section('title', 'Todas las tareas')

@section('content')
    <div class="main-buttons">
        <form method="get" action="{{route('main.tasks.create')}}">
        <button type="submit" class="btn btn-success">
            <img src= {{ @asset('/svg/plus.svg') }} alt="Agregar nueva tarea">
        </button>
        </form>
    </div>
    <hr>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Alias</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripción</th>
                <th scope="col">Estado</th>
                <th scope="col">Fecha inicial</th>
                <th scope="col">Fecha final</th>
                <th scope="col">Usuarios asignados</th>
                <th scope="col">Editar</th>
                <th scope="col">Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tasks as $task)
                <tr>
                    <th scope="row">{{ $task->alias }}</th>
                    <td>{{ $task->name }}</td>
                    <td>{{ $task->description }}</td>
                    <td>{{ $task->status }}</td>
                    <td>{{ $task->initial_date }}</td>
                    <td>{{ $task->final_date }}</td>
                    <td>
                        @foreach ($task->user as  $asigned_user)
                            {{ $asigned_user->name }}
                        @endforeach
                    </td>
                    <td>
                        <a href="{{ route('main.tasks.edit', $task->id) }}">
                            <button class="btn btn-info">
                                <img src= {{ @asset('/svg/pencil.svg') }} alt="Editar">
                            </button>
                        </a>
                    </td>
                    <td>
                        <form action="{{ route('main.tasks.destroy', $task->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">
                                <img src= {{ @asset('/svg/trash.svg') }} alt="Eliminar">
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination_links row">
            {{ $tasks->links() }}
    </div>
@endsection