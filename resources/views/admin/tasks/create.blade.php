@extends('admin.main')

@section('title', 'Creacion de tareas')

@section('content')

    @if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $e)
            <li>{{$e}}</li>
        @endforeach
    </div>
    @endif
    
    {!! Form::open(['route' => 'main.tasks.store', 'method' => 'POST']) !!}
        <div class="form-group">
            {!! Form::label('alias', 'Alias') !!}
            {!! Form::text('alias',null, ['class' => 'form-control', 'placeholder' => 'Ingrese un alias (único)', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('name',null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripcion') !!}
            {!! Form::text('description',null, ['class' => 'form-control', 'placeholder' => 'Ingrese la descripción', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Estado') !!}
            {!! Form::text('status',null, ['class' => 'form-control', 'placeholder' => 'Ingrese el estado', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('initial_date', 'Fecha inicial') !!}
            {!! Form::date('initial_date',null, ['class' => 'form-control', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('final_date', 'Fecha final') !!}
            {!! Form::date('final_date',null, ['class' => 'form-control', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}
@endsection