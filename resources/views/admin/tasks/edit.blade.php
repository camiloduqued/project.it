@extends('admin.main')

@section('title', $task->name)

@section('content')

    @if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $e)
            <li>{{$e}}</li>
        @endforeach
    </div>
    @endif
    
    {!! Form::open(['route' => ['tasks.update', $task], 'method' => 'PUT']) !!}
        <div class="form-group">
            {!! Form::label('alias', 'Alias') !!}
            {!! Form::text('alias',$task->alias, ['class' => 'form-control', 'placeholder' => 'Ingrese un alias (único)', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('name',$task->name, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripcion') !!}
            {!! Form::text('description',$task->description, ['class' => 'form-control', 'placeholder' => 'Ingrese la descripción', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Estado') !!}
            {!! Form::text('status',$task->status, ['class' => 'form-control', 'placeholder' => 'Ingrese el estado', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('initial_date', 'Fecha inicial') !!}
            {!! Form::date('initial_date',$task->initial_date, ['class' => 'form-control', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('final_date', 'Fecha final') !!}
            {!! Form::date('final_date',$task->final_date, ['class' => 'form-control', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}
@endsection