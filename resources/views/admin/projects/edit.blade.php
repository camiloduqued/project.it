@extends('admin.main')

@section('title', $project->name)

@section('content')

    @if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $e)
            <li>{{$e}}</li>
        @endforeach
    </div>
    @endif
    
    {!! Form::open(['route' => ['projects.update', $project], 'method' => 'PUT', 'files' => true]) !!}
        <div class="form-group">
            {!! Form::label('alias', 'Alias') !!}
            {!! Form::text('alias',$project->alias, ['class' => 'form-control', 'placeholder' => 'Ingrese un alias (único)', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('name',$project->name, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripcion') !!}
            {!! Form::text('description',$project->description, ['class' => 'form-control', 'placeholder' => 'Ingrese la descripción', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Estado') !!}
            {!! Form::text('status',$project->status, ['class' => 'form-control', 'placeholder' => 'Ingrese el estado', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('initial_date', 'Fecha inicial') !!}
            {!! Form::date('initial_date',$project->initial_date, ['class' => 'form-control', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('final_date', 'Fecha final') !!}
            {!! Form::date('final_date',$project->final_date, ['class' => 'form-control', 'required'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::label('leader_user_id', 'Lider del proyecto') !!}
            {!! Form::select('leader_user_id', $options, $project->leader_user_id) !!}
        </div>
        <div class="form-group">
            {!! Form::label('avatar', 'Avatar') !!}
            {!! Form::file('avatar'); !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Agregar', ['class'=>'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}
@endsection