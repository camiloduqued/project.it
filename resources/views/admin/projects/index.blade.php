@extends('admin.main')
@section('title', 'Todos los proyectos')

@section('content')
    <div class="main-buttons">
        <form method="get" action="{{route('projects.create')}}">
        <button type="submit" class="btn btn-success">
            <img src= {{ @asset('/svg/plus.svg') }} alt="Agregar nuevo proyecto">
        </button>
        </form>
    </div>
    <hr>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Alias</th>
                <th scope="col">Avatar</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripción</th>
                <th scope="col">Estado</th>
                <th scope="col">Lider</th>
                <th scope="col">Fecha inicial</th>
                <th scope="col">Fecha final</th>
                <th scope="col">Editar</th>
                <th scope="col">Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($projects as $project)
                <tr>
                    <th scope="row">{{ $project->alias }}</th>
                    <td><img class="avatar_img" src="/imgs/project/{{ $project->avatar }}" alt=""></td>
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->description }}</td>
                    <td>{{ $project->status }}</td>
                    <td>{{ $project->user->name }}</td>
                    <td>{{ $project->initial_date }}</td>
                    <td>{{ $project->final_date }}</td>
                    <td>
                        <a href="{{ route('projects.edit', $project->id) }}">
                            <button class="btn btn-info">
                                <img src= {{ @asset('/svg/pencil.svg') }} alt="Editar">
                            </button>
                        </a>
                    </td>
                    <td>
                        <form action="{{ route('projects.destroy', $project->id) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">
                                <img src= {{ @asset('/svg/trash.svg') }} alt="Eliminar">
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pagination_links row">
            {{ $projects->links() }}
    </div>
@endsection