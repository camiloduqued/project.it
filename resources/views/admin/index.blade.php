@extends('admin.main')

@section('content')

<div id="index_admin_categories">
    <p class="title">¿Qué quieres administrar?</p>
    <div class="links">
        <a href="{{ route('users.index') }}">Usuarios</a>
        <a href="{{ route('projects.index') }}">Proyectos</a>
        <a href="{{ route('tasks.index') }}">Tareas</a>
    </div>
</div>

@endsection