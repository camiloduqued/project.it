<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{   //Mass assignable attributes.
    protected $fillable = ['title', 'content', 'tags'];
    // Name in MySQL table
    protected $table = 'comments';

    public function comment(){
        return $this->hasMany('App\Comment', 'parent_comment_id');
    }

    public function tag(){
        return $this->hasMany('App\Tag');
    }

    public function task(){
        return $this->belongsTo('App\Task', 'parent_task_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
