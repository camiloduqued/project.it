<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //Mass assignable attributes.
    protected $fillable = ['alias', 'name', 'description', 'status', 'initial_date', 'final_date'];
    // Name in MySQL table
    protected $table = 'tasks';

    public function comment(){
        return $this->hasMany('App\Comment', 'parent_task_id');
    }

    public function task (){
        return $this->hasMany('App\Task', 'parent_task_id');
    }

    public function user(){
        return $this->belongsToMany('App\User', 'user_task', 'task_id', 'user_id');
    }
}
