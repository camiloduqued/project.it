<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //Mass assignable attributes.
    protected $fillable = ['content'];

    // Name in MySQL table
    protected $table = 'tags';

    public function comment(){
        return $this->belongsTo('App\Comment');
    }
}
