<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{   //Mass assignable attributes.
    protected $fillable = ['alias', 'name', 'description', 'avatar', 'status', 'initial_date', 'final_date', 'leader_user_id'];
    // Name in MySQL table
    protected $table = 'projects';

    public function user(){
        return $this->belongsTo('App\User', 'leader_user_id');
    }
}
