<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectRequest;
use Auth;
use App\Project;
use \Datetime;

class MyProjectsController extends Controller
{
    public function index(){
        $myProjects = Auth::user()->project->all();
        return view('main.projects.index')->with('myProjects', $myProjects);
    }

    public function create(){
        return view('main.projects.create');
    }

    public function store(ProjectRequest $request){

        $initialDate = new DateTime($request->initial_date);
        $finalDate = new DateTime($request->final_date);

        if($initialDate < $finalDate){
            $project = new Project($request->all());
            if($request->file('avatar')){
                $file = $request->file('avatar');
                $name = 'project_avatar'.time().'.'.$file->getClientOriginalExtension();
                $path = public_path().'/imgs/project/';
                $file->move($path, $name);
                $project->avatar = $name;
            }
            $project->leader_user_id = Auth::user()->id;
            $project->save();

            return redirect()->route('main.projects.index');
        } else {
            return redirect()->route('main.projects.create')->withErrors('La fecha inicial debe ser anterior a la final.');
        }
    }

    public function edit($id){
        $project = Project::find($id);
        return view('main.projects.edit')->with(['project' => $project]);
    }

    public function update($id, Request $request){
        
        $project = Project::find($id);
        $aux = $project->avatar;
        $project->fill($request->all());
        $project->avatar = $aux;

        if($request->file('avatar')){
            $file = $request->file('avatar');
            $name = 'project_avatar'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/imgs/project/';
            unlink($path.$project->avatar);
            $file->move($path, $name);
            $project->avatar = $name;
        }
       
        $project->save();
        return redirect()->route('main.projects.index');
    }

    public function destroy($id){
        $project = Project::find($id);
        if($project->leader_user_id == Auth::user()->id){
            $path = public_path().'/imgs/project/';
            unlink($path.$project->avatar);
            $project->delete();
            return redirect()->route('main.projects.index');
        } else {
            dd('Error: está intentando eliminar un proyecto que no es suyo.');
        }
    }
}
