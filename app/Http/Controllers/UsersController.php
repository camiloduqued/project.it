<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;

class UsersController extends Controller
{
    public function index(){
        $users = User::orderBy('name', 'asc')->paginate(5);
        return view('admin.users.index')->with('users', $users);
    }

    public function create(){
        return view('admin.users.create');
    }

    public function store(UserRequest $request){

        $user = new User($request->all());

        if($request->file('avatar')){
            $file = $request->file('avatar');
            $name = 'user_avatar'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/imgs/user/';
            $file->move($path, $name);
            $user->avatar = $name;
        }

        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('users.index');
    }

    public function edit($id){
        $user = User::find($id);
        return view('admin.users.edit')->with('user', $user);
    }

    public function update($id, Request $request){
        $user = User::find($id);
        $aux = $user->avatar;
        $user->fill($request->all());
        $user->avatar = $aux;

        if($request->file('avatar')){
            $file = $request->file('avatar');
            $name = 'user_avatar'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/imgs/user/';
            unlink($path.$user->avatar);
            $file->move($path, $name);
            $user->avatar = $name;
        }

        $user->admin = (isset($request->admin) ? true : false);
       
        $user->save();
        return redirect()->route('users.index');
    }

    public function destroy($id){
        $user = User::find($id);
        $path = public_path().'/imgs/user/';
        unlink($path.$user->avatar);
        $user->delete();

        return redirect()->route('users.index');
    }
}
