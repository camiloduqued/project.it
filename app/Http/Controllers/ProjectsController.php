<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\User;
use App\Http\Requests\ProjectRequest;
use \DateTime;

class ProjectsController extends Controller
{
    public function index(){
        $projects = Project::orderBy('name', 'asc')->paginate(5);
        return view('admin.projects.index')->with('projects', $projects);
    }

    public function create(){
        
        $users = User::orderBy('name', 'asc')->get();
        $options = array();
        foreach ($users as $user) {
            $options[$user->id] = $user->name;
        }
        return view('admin.projects.create', ['options'=>$options]);
    }

    public function store(ProjectRequest $request){

        $initialDate = new DateTime($request->initial_date);
        $finalDate = new DateTime($request->final_date);

        if($initialDate < $finalDate){
            $project = new Project($request->all());
            if($request->file('avatar')){
                $file = $request->file('avatar');
                $name = 'project_avatar'.time().'.'.$file->getClientOriginalExtension();
                $path = public_path().'/imgs/project/';
                $file->move($path, $name);
                $project->avatar = $name;
            }
            $project->save();

            return redirect()->route('projects.index');
        }else{
            return redirect()->route('projects.create')->withErrors('La fecha inicial debe ser anterior a la final.');
        }
    }

    public function edit($id){
        $users = User::orderBy('name', 'asc')->get();
        $options = array();
        foreach ($users as $user) {
            $options[$user->id] = $user->name;
        }
        $project = Project::find($id);
        return view('admin.projects.edit')->with(['project' => $project, 'options' => $options]);
    }

    public function update($id, Request $request){
        $project = Project::find($id);
        $aux = $project->avatar;
        $project->fill($request->all());
        $project->avatar = $aux;

        if($request->file('avatar')){
            $file = $request->file('avatar');
            $name = 'project_avatar'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/imgs/project/';
            unlink($path.$project->avatar);
            $file->move($path, $name);
            $project->avatar = $name;
        }
       
        $project->save();
        return redirect()->route('projects.index');
    }

    public function destroy($id){
        $project = Project::find($id);
        $path = public_path().'/imgs/project/';
        unlink($path.$project->avatar);
        $project->delete();

        return redirect()->route('projects.index');
    }
}
