<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class MyUserController extends Controller
{
    public function update(Request $request){
        $user = User::find(Auth::user()->id);

        if($request->file('avatar')){
            $file = $request->file('avatar');
            $name = 'user_avatar'.time().'.'.$file->getClientOriginalExtension();
            $path = public_path().'/imgs/user/';
            $file->move($path, $name);
            $user->avatar = $name;
        }
       
        $user->save();
        return redirect()->route('home');
    }
}
