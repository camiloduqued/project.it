<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\TaskRequest;
use App\Task;
use App\User;
use App\Comment;
use App\Tag;
use \DateTime;
use Illuminate\Support\Facades\DB;

class MyTasksController extends Controller
{
    public function index(){
        $myTasks = Auth::user()->task->all();
        return view('main.tasks.index')->with('myTasks', $myTasks);
    }
    
    public function create(){
        return view('main.tasks.create');
    }

    public function store(TaskRequest $request){
        $initialDate = new DateTime($request->initial_date);
        $finalDate = new DateTime($request->final_date);

        if($initialDate < $finalDate){
            $task = new Task($request->all());
            $task->save();
            $aux = Task::find($task->id);
            $aux->user()->attach(Auth::user()->id);

            return redirect()->route('main.tasks.index');
        }else {
            return redirect()->route('main.tasks.create')->withErrors('La fecha inicial debe ser anterior a la final.');
        }
    }

    public function edit($id){

        $userTask = DB::table('user_task')->where(['user_id'=>Auth::user()->id, 'task_id' => $id])->first();
        //dd($userTask);
        $task = Task::find($id);
        $users = User::orderBy('name', 'asc')->get();
        $options = array();
        foreach ($users as $user) {
            $options[$user->id] = $user->name;
        }
        return view('main.tasks.edit')->with(['task' => $task, 'options'=> $options, 'user_task'=>$userTask]);
    }

    public function update($id, Request $request){
        $task = Task::find($id);
        $task->fill($request->all());
       
        $task->save();
        return redirect()->route('main.tasks.index');
    }

    public function attachUser(Request $request){
        $task = Task::find($request->task_id);
        $task->user()->attach($request->user_to_attach);
        return redirect()->route('main.tasks.index');
    }

    public function comment(Request $request){
        
        $comment = new Comment();
        $comment->title = $request->title;
        $comment->content = $request->content;
        $comment->user_id = Auth::user()->id;
        $comment->parent_task_id = $request->task_id;
        $comment->save();

        $tagsArray = explode(',', $request->tags);
        foreach ($tagsArray as $tag) {
            $auxTag = new Tag();
            $auxTag->content = $tag;
            $auxTag->comment_id = $comment->id;
            $auxTag->save();
        }

        return redirect()->route('main.tasks.index');
    }

    public function destroy($id){
        $task = Task::find($id);
        $task->delete();

        return redirect()->route('main.tasks.index');
    }

    public function setProgress(Request $request){
        $task = Task::find($request->task_id);
        $initialDate = new DateTime($task->initial_date);
        $finalDate = new DateTime($task->final_date);
        $difference = $initialDate->diff($finalDate);
        $hoursToWork = $difference->format('%H');
        $intervalInSeconds = (new DateTime())->setTimeStamp(0)->add($difference)->getTimeStamp();
        $intervalInHours = $intervalInSeconds/60/60;
        $percentage = ($request->progress_time/$intervalInHours)*100;
        $percentage = round($percentage);
        $userTask = DB::table('user_task')
                    ->where('id', $request->user_task_id)
                    ->update(['progress_time' => $request->progress_time, 'progress_percentage'=> $percentage]);

        return redirect()->route('main.tasks.edit', $request->task_id);
                
    }
}
