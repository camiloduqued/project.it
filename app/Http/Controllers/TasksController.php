<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Requests\TaskRequest;
use \DateTime;

class TasksController extends Controller
{
    public function index(){
        $tasks = Task::orderBy('name', 'asc')->paginate(5);
        return view('admin.tasks.index')->with('tasks', $tasks);
    }

    public function create(){
        return view('admin.tasks.create');
    }

    public function store(TaskRequest $request){
        $initialDate = new DateTime($request->initial_date);
        $finalDate = new DateTime($request->final_date);

        if($initialDate < $finalDate){
            $task = new Task($request->all());
            $task->save();

            return redirect()->route('tasks.index');
        }else{
            return redirect()->route('tasks.create')->withErrors('La fecha inicial debe ser anterior a la final.');
        }
    }

    public function edit($id){
        $task = Task::find($id);
        return view('admin.tasks.edit')->with('task', $task);
    }

    public function update($id, Request $request){
        $task = Task::find($id);
        $task->fill($request->all());
       
        $task->save();
        return redirect()->route('tasks.index');
    }

    public function destroy($id){
        $task = Task::find($id);
        $task->delete();

        return redirect()->route('tasks.index');
    }
}
