<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Name in MySQL table
    protected $table = 'users';

    public function project(){
        return $this->hasMany('App\Project', 'leader_user_id');
    }

    public function comment(){
        return $this->hasMany('App\Comment');
    }

    public function task(){
        return $this->belongsToMany('App\Task', 'user_task', 'user_id', 'task_id');
    }
}
